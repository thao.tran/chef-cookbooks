#
# Cookbook Name:: ez-sudo
# Recipe:: default
#
# Maintainer:: Thao Tran
# Copyright 2018, Ez LLC.
# All rights reserved - Do Not Redistribute
#

cookbook_file "/etc/sudoers.d/ezadmin" do
   source "ezadmin"
   owner  "root"
   group  "root"
   mode   "0440"
end
