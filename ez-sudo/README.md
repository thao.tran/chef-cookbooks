ez-sudo Cookbook
===================
Add ezadmin ldap group to sudoers.

Requirements
------------
sudo

Attributes
----------

e.g.
#### ez-sudo::default
<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['ez-sudo']['bacon']</tt></td>
    <td>Boolean</td>
    <td>whether to include bacon</td>
    <td><tt>true</tt></td>
  </tr>
</table>

Usage
-----
#### ez-sudo::default

e.g.
Just include `ez-sudo` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[ez-sudo]"
  ]
}
```
