# cookbooks repo
README File for Cookbooks Repo

Date: 06-May-2019<br>
Maintainer: Thao Tran (<thao.tran@ez.com>)

---
<br>
<br>

## Prerequisites:
  
1. Chef Infra Server 12.17.15
<br> https://downloads.chef.io/chef-server/stable/12.17.15
  
2. Chef Infra Client 15.5.15
<br> https://downloads.chef.io/chef/stable/15.5.15

## Usage:
- Installing Chef Infra Server package in a node to promote it becoming Chef Server node.
- Installing Chef Infra Client package in all app nodes or provisioning nodes to work with Chef server.
- Upload all cookbooks and depends cookbooks to Chef Server.
- Create chef admin user, valiator.pem key, client.rb file in provisioning nodes.
- Create new chef-environment. Ex: `devsapp_di01`