ez-tweak Cookbook
====================
This cookbook tweaks things

Requirements
------------

#### packages
- `limits` - ez-tweak adds limits files to /etc/security/limits.conf.d/

Attributes
----------
#### ez-tweak::limits
<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['ez-tweak']['limits'][USER]</tt></td>
    <td>Array</td>
    <td>Array of arrays to be put in limits.conf for user USER</td>
    <td><tt>nil</tt></td>
  </tr>
</table>

Usage
-----
#### ez-tweak::limits

Just include `ez-tweak::limits` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[ez-tweak::limits]"
  ]
}
```

Contributing
------------

1. Fork the repository on Github
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request using Github

License and Authors
-------------------

Maintainer:: Thao Tran (<thao.tran@ez.com>)
