name             'ez-tweak'
maintainer       'Thao Tran'
maintainer_email 'thao.tran@ez.com'
license          'All rights reserved'
description      'Installs/Configures itson-tweak'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.2.1'
