#
# Cookbook Name:: itson-tweak
# Recipe:: limits
#
# Copyright 2014, ItsOn, Inc.
#
# All rights reserved - Do Not Redistribute
#

node['itson-tweak']['limits'].each_pair do |name,v|
  v.each_pair do |num,confs|
    template "/etc/security/limits.d/#{num}-#{name}.conf" do
      source "limits.conf.erb"
      mode 0644
      owner "root"
      group "root"
      variables ({
        :name   => name,
        :values => confs
      })
    end
  end
end
