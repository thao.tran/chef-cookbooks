#
# Cookbook Name:: ez-tweak
# Recipe:: ez_alias-ps1
#
# Maintainer:: Thao Tran
# Copyright 2018, Ez LLC.
# All rights reserved - Do Not Redistribute
#

if node['fqdn'].include? "devsapp" or node['ez_alias'].nil?
  node.default['ez_alias'] = node['fqdn'].gsub /.devsapp*/, ""
end

node['ez-tweak']['ez_alias-ps1'].each_pair do |name,value|
  template "/etc/profile.d/ez_alias_#{name}-ps1.sh" do
    mode 00644
    source 'ez_alias-ps1.sh.erb'
    variables({
      :name => name,
      :value => value
    })
    only_if { node['ez_alias'] }
  end
end
