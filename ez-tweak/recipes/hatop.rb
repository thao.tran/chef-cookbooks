#
# Cookbook Name:: ez-tweak
# Recipe:: hatop
#
# Maintainer:: Thao Tran
# Copyright 2018, Ez LLC.
# All rights reserved - Do Not Redistribute
#

# convenient wrapper for executing hatop
# this will run the following:
# $ sudo -u haproxy /usr/local/bin/hatop -ns /var/run/sock-haproxy
# the original can still be run with:
# $ /usr/local/bin/hatop

if node['ez-haproxy']['hatop']['enable'] == true
  cookbook_file "/etc/profile.d/hatop.sh" do
    mode 00644
    source 'hatop.sh'
  end
end
