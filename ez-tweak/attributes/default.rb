#
# Cookbook Name:: ez-tweak
# Attribute:: default
#
# Maintainer:: Thao Tran
# Copyright 2018, Ez LLC.
# All rights reserved - Do Not Redistribute
#

default['ez-tweak']['limits']                = {}
default['ez-tweak']['ez_alias-ps1']['all'] = '[\u@${EZ_HOST} \W]\\$ '
