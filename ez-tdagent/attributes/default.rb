#
# Cookbook Name:: ez-tdagent
# Attributes:: default
# Maintainer:: Thao Tran
# Copyright 2019, Ez, LLC.
# All rights reserved - Do Not Redistribute
#

default["ez-tdagent"]["api_key"]               = ''
default["ez-tdagent"]["plugins"]               = []
default["ez-tdagent"]["uid"]                   = nil
default["ez-tdagent"]["gid"]                   = nil
default["ez-tdagent"]["user"]                  = 'td-agent'
default["ez-tdagent"]["group"]                 = 'td-agent'
default["ez-tdagent"]["includes"]              = true
default["ez-tdagent"]["default_config"]        = true
default["ez-tdagent"]["fe_config"]             = ['home','subscriber','partner','notify']
default["ez-tdagent"]["queue_config"]          = ['ums','plancycle','notify','iop']
default["ez-tdagent"]["template_cookbook"]     = 'ez-tdagent'
default["ez-tdagent"]["in_http"]["enable_api"] = true
default["ez-tdagent"]["version"]               = "3.5.0-0.el6"
default["ez-tdagent"]["pinning_version"]       = true
default["ez-tdagent"]["apt_arch"]              = 'amd64'
default["ez-tdagent"]["in_forward"]            = { port: 24224, bind: '0.0.0.0' }
default["ez-tdagent"]["in_http"]               = { port: 8888, bind: '0.0.0.0' }
default['ez-tdagent']['skip_repository']       = false
default['ez-tdagent']['node_ip']               = node['ipaddress']
default['ez-tdagent']['node_name']             = node['io_alias']
default['ez-tdagent']['node_di']               = node['io_di']
default['ez-tdagent']['node_env']              = node['io_env']