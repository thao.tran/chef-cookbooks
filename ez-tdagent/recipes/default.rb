#
# Cookbook Name:: ez-tdagent
# Recipe:: default
# Maintainer:: Thao Tran
# Copyright 2019, Ez, LLC.
# All rights reserved - Do Not Redistribute
#

include_recipe 'ez-tdagent::install'
include_recipe 'ez-tdagent::configure'
