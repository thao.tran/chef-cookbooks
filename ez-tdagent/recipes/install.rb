#
# Cookbook Name:: ez-tdagent
# Recipe:: install
# Maintainer:: Thao Tran
# Copyright 2019, Ez, LLC.
# All rights reserved - Do Not Redistribute
#

#grok_parser = File.join( Chef::Config[:file_cache_path], 'cookbooks', cookbook_name, 'files', 'default', 'fluent-plugin-grok-parser-2.1.4.gem' )

%w{fluent-plugin-grok-parser-2.1.4.gem fluent-plugin-multi-format-parser-1.0.0.gem}.each do |plugin|
  cookbook_file "/tmp/#{plugin}" do
      source plugin
      owner "root"
      group "root"
      mode "0750"
      action :create
  end
end

group node["ez-tdagent"]["group"] do
  group_name node["ez-tdagent"]["group"]
  gid node["ez-tdagent"]["gid"] if node["ez-tdagent"]["gid"]
  system true
  action [:create]
end

user node["ez-tdagent"]["user"] do
  comment 'td-agent'
  uid node["ez-tdagent"]["uid"] if node["ez-tdagent"]["uid"]
  system true
  group  node["ez-tdagent"]["group"]
  home   '/var/run/td-agent'
  shell  '/bin/false'
  password nil
  manage_home true
  action [:create, :manage]
end

directory '/var/run/td-agent/' do
  owner  node["ez-tdagent"]["user"]
  group  node["ez-tdagent"]["group"]
  mode   "0755"
  action :create
end

directory '/etc/td-agent/' do
  owner  node["ez-tdagent"]["user"]
  group  node["ez-tdagent"]["group"]
  mode   "0755"
  action :create
end

directory "/etc/td-agent/conf.d" do
  owner  node["ez-tdagent"]["user"]
  group  node["ez-tdagent"]["group"]
  mode   "0755"
  only_if { node["ez-tdagent"]["includes"] }
end

package "td-agent" do
  retries 3
  retry_delay 10
  if node["ez-tdagent"]["pinning_version"]
    action :install
    version node["ez-tdagent"]["version"]
  end
end

gem_package "fluent-plugin-grok-parser" do
  gem_binary "/opt/td-agent/embedded/bin/gem"
  source "/tmp/fluent-plugin-grok-parser-2.1.4.gem"
  #source grok_parser
  action :install
end

gem_package "fluent-plugin-multi-format-parser" do
  gem_binary "/opt/td-agent/embedded/bin/gem"
  source "/tmp/fluent-plugin-multi-format-parser-1.0.0.gem"
  action :install
end
