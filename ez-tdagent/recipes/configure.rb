#
# Cookbook Name:: ez-tdagent
# Recipe:: configure
# Maintainer:: Thao Tran
# Copyright 2019, Ez, LLC.
# All rights reserved - Do Not Redistribute
#

Chef::Recipe.send(:include, TdAgent::Version)
Chef::Provider.send(:include, TdAgent::Version)

reload_action = (reload_available?) ? :reload : :restart

major_version = major

unless ["sapp-operator", "portal"].any? { |app| node['ez-tdagent']['node_name'].include? app }
  template "/etc/td-agent/td-agent.conf" do
    owner  node["ez-tdagent"]["user"]
    group  node["ez-tdagent"]["group"]
    mode "0644"
    cookbook node['ez-tdagent']['template_cookbook']
    source "td-agent.conf.erb"
    variables(
      :major_version => major_version
    )
    notifies reload_action, "service[td-agent]", :delayed
  end
end

if ["fe-partner","fe-notify","fe-home","fe-subscriber","fe-sapp-adapter","fe-sapp-operator"].any? { |app| node['ez-tdagent']['node_name'].include? app }
  template "/etc/td-agent/conf.d/td-agent.conf" do
    owner  "root"
    group  "root"
    mode "0644"
    cookbook node['ez-tdagent']['template_cookbook']
    source "fe_td-agent.conf.erb"
    variables(
      :major_version => major_version
    )
    notifies reload_action, "service[td-agent]", :delayed
  end
end

if ["notify","plancyle","ums","iop"].any? { |app| node['ez-tdagent']['node_name'].include? app }
  template "/etc/td-agent/conf.d/queue.conf" do
    owner  "root"
    group  "root"
    mode "0644"
    cookbook node['ez-tdagent']['template_cookbook']
    source "queue_td-agent.conf.erb"
    variables(
      :major_version => major_version
    )
    notifies reload_action, "service[td-agent]", :delayed
  end
end

if node['ez-tdagent']['node_name'].include? "sapp-adapter"
  template "/etc/td-agent/conf.d/adapters_td-agent.conf" do
    owner  "root"
    group  "root"
    mode "0644"
    cookbook node['ez-tdagent']['template_cookbook']
    source "adapters_td-agent.conf.erb"
    variables(
      :major_version => major_version
    )
    notifies reload_action, "service[td-agent]", :delayed
  end  
end

if node['ez-tdagent']['node_name'].include? "sapp-operator"
  template "/etc/td-agent/td-agent.conf" do
    owner  node["ez-tdagent"]["user"]
    group  node["ez-tdagent"]["group"]
    mode "0644"
    cookbook node['ez-tdagent']['template_cookbook']
    source "op_td-agent.conf.erb"
    variables(
      :major_version => major_version
    )
    notifies reload_action, "service[td-agent]", :delayed
  end
end

if node['ez-tdagent']['node_name'].include? "portal"
  template "/etc/td-agent/td-agent.conf" do
    owner  node["ez-tdagent"]["user"]
    group  node["ez-tdagent"]["group"]
    mode "0644"
    cookbook node['ez-tdagent']['template_cookbook']
    source "portal-myaccount_td-agent.conf.erb"
    variables(
      :major_version => major_version
    )
    notifies reload_action, "service[td-agent]", :delayed
  end  
end

node["ez-tdagent"]["plugins"].each do |plugin|
  if plugin.is_a?(Hash)
    plugin_name, plugin_attributes = plugin.first
    td_agent_gem plugin_name do
      plugin true
      %w{action version source options gem_binary}.each do |attr|
        send(attr, plugin_attributes[attr]) if plugin_attributes[attr]
      end
      notifies :restart, "service[td-agent]", :delayed
    end
  elsif plugin.is_a?(String)
    td_agent_gem plugin do
      plugin true
      notifies :restart, "service[td-agent]", :delayed
    end
  end
end

service "td-agent" do
  supports :restart => true, :reload => (reload_action == :reload), :status => true
  restart_command "/etc/init.d/td-agent restart || /etc/init.d/td-agent start"
  action [ :enable, :start ]
end

