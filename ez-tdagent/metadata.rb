name             "ez-tdagent"
maintainer       "Thao Tran"
maintainer_email "thao.tran@ez.com"
license          "Apache 2.0"
description      "Installs/Configures td-agent"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "1.1.2"

%w{redhat centos debian ubuntu}.each do |os|
  supports os
end

depends 'apt'
depends 'yum'
