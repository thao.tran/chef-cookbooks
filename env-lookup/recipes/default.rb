#
# Cookbook Name:: env-lookup
# Recipe:: default
# Maintainer:: Thao Tran
# Copyright 2019, Ez, LLC.
# All rights reserved - Do Not Redistribute
#

include_recipe "env-lookup::common"

ez_env = node['ez_env']
include_recipe "env-#{ez_env}"
