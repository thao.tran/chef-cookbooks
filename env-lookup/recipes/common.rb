#
# Cookbook Name:: env-lookup
# Recipe:: common
# Maintainer:: Thao Tran
# Copyright 2019, Ez, LLC.
# All rights reserved - Do Not Redistribute
#

# Determine the node's environment and deployment instance
# If ez_env or ez_di is not set, infer chef_environment

ez_env = node['ez_env']

unless ez_env
  Chef::Log.info 'ez_env not set. Inferring from chef_environment'
  ez_env = node.chef_environment.split('_')[0]
  node.override['ez_env'] = ez_env
  Chef::Log.info "ez_env set to #{ez_env}"
  raise 'ez_env attribute must be set' if ez_env.empty?
end
