name             'env-lookup'
maintainer       'Thao Tran'
maintainer_email 'thao.tran@ez.com'
license          'All rights reserved'
description      'Cookbook to manage environment lookup.  This is the prefered method for loading attributes based on the environment of the node'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.1'

depends 'env-devsapp'
