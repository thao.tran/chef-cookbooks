
#
# Cookbook Name:: ez-yum
# Recipe:: ambari
# Maintainer:: Thao Tran
# Copyright 2018, Ez LLC.
# All rights reserved - Do Not Redistribute
#

# This should really only be used on the repo server to configured a reposync

include_recipe 'ez-yum::default'

# The ambari repo is Updates
yum_repository "Updates-ambari-#{node['ez-yum']['ambari_version']}" do
  description "ambari-#{node['ez-yum']['ambari_version']} - Updates"
  baseurl "http://public-repo-1.hortonworks.com/ambari/centos6/2.x/updates/#{node['ez-yum']['ambari_version']}"
  gpgcheck false
  enabled true
  action :create
end
