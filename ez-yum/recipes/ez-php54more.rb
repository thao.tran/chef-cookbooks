#
# Cookbook Name:: ez-yum
# Recipe:: ez-php54
#
# Maintainer:: Thao Tran
# Copyright 2018, Ez LLC.
# All rights reserved - Do Not Redistribute
#

include_recipe "ez-yum::default"

# includes xdebug pacakge for php54
yum_repository 'php54more' do
  description 'Remi php54more Repo'
  baseurl "#{node['sysadmin']['yum_repo_url']}/remi-php54more-epel-6-x86_64"
  gpgcheck false
  enabled  true
  action   :create
end
