#
# Cookbook Name:: ez-yum
# Recipe:: bintray
# Maintainer:: Thao Tran
# Copyright 2018, Ez LLC.
# All rights reserved - Do Not Redistribute
#
node['ez-yum']['bintray_repos'].each do |repo|
  yum_repository node['ez-yum'][repo]['name'] do
    description "Ez Repo for #{repo} RPMs"
    baseurl node['ez-yum'][repo]['baseurl']
    metadata_expire node['ez-yum']['metadata_expire']
    gpgcheck node['ez-yum'][repo]['gpgcheck']
    action   :create
  end
end
