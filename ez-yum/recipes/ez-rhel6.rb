#
# Cookbook Name:: ez-yum
# Recipe:: ez-rhel6
#
# Maintainer:: Thao Tran
# Copyright 2018, Ez LLC.
# All rights reserved - Do Not Redistribute
#

include_recipe "ez-yum::default"

execute 'enable/disable rhsm.manage_repos' do
  command "subscription-manager config --rhsm.manage_repos=#{node['ez-yum']['rhsm_manage_repos']}"
end

yum_repository 'ez-rhel-6-server-rpms' do
  description 'rhel-6-server-rpms local mirror'
  baseurl "#{node['sysadmin']['yum_repo_url']}/rhel-6-server-rpms/"
  gpgcheck false
  enabled  true
  action   :create
end

yum_repository 'ez-rhel-6-server-optional-rpms' do
  description 'rhel-6-server-optional-rpms local mirror'
  baseurl "#{node['sysadmin']['yum_repo_url']}/rhel-6-server-optional-rpms/"
  gpgcheck false
  enabled  true
  action   :create
end
