#
# Cookbook Name:: ez-yum
# Recipe:: ez-php55
#
# Maintainer:: Thao Tran
# Copyright 2018, Ez LLC.
# All rights reserved - Do Not Redistribute
#

yum_repository 'ez-rhscl-php55' do
  description 'Ez rhscl-php55 local mirror - $basearch'
  baseurl "#{node['sysadmin']['yum_repo_url']}/ez-rhscl-php55-epel-6-x86_64/"
  gpgcheck false
  metadata_expire node['ez-yum']['metadata_expire']
  enabled  true
  action   :create
end

yum_repository 'ez-remi-php55more' do
  description 'Ez remi-php55more local mirror - $basearch'
  baseurl "#{node['sysadmin']['yum_repo_url']}/ez-remi-php55more-epel-6-x86_64/"
  gpgcheck false
  metadata_expire node['ez-yum']['metadata_expire']
  enabled  true
  action   :create
end