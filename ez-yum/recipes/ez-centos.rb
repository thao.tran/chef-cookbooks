#
# Cookbook Name:: ez-yum
# Recipe:: ez-centos
#
# Maintainer:: Thao Tran
# Copyright 2018, Ez LLC.
# All rights reserved - Do Not Redistribute
#

yum_repository 'ez-centos6' do
  description 'Ez centos6-rhel local mirror - $basearch'
  baseurl "#{node['sysadmin']['yum_repo_url']}/CentOS6/"
  gpgcheck false
  metadata_expire node['ez-yum']['metadata_expire']
  enabled  true
  action   :create
end

yum_repository 'ez-centos6-rhel' do
  description 'Ez centos6-rhel local mirror - $basearch'
  baseurl "#{node['sysadmin']['yum_repo_url']}/CentOS6-RHEL/"
  gpgcheck false
  metadata_expire node['ez-yum']['metadata_expire']
  enabled  true
  action   :create
end

yum_repository 'ez-centos6-scl' do
  description 'Ez centos6-scl local mirror - $basearch'
  baseurl "#{node['sysadmin']['yum_repo_url']}/CentOS6-SCL/"
  gpgcheck false
  metadata_expire node['ez-yum']['metadata_expire']
  enabled  true
  action   :create
end

yum_repository 'ez-rhscl-python27' do
  description 'Ez rhscl-python27-epel-6-x86_64 local mirror - $basearch'
  baseurl "#{node['sysadmin']['yum_repo_url']}/ez-rhscl-python27-epel-6-x86_64/"
  gpgcheck false
  metadata_expire node['ez-yum']['metadata_expire']
  enabled  true
  action   :create
end
