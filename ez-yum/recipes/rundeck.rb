#
# Cookbook Name:: ez-yum
# Recipe:: rundeck
#
# Maintainer:: Thao Tran
# Copyright 2018, Ez LLC.
# All rights reserved - Do Not Redistribute
#
#
include_recipe "ez-yum::default"

yum_repository 'rundeck' do
  description 'Rundeck Repo'
  baseurl "http://dl.bintray.com/rundeck/rundeck-rpm"
  gpgcheck false
  enabled  true
  action   :create
end
