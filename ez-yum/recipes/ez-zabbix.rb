#
# Cookbook Name:: ez-yum
# Recipe:: ez-zabbix
#
# Maintainer:: Thao Tran
# Copyright 2018, Ez LLC.
# All rights reserved - Do Not Redistribute
#

yum_repository 'Ez-Zabbix' do
  description 'Ez Zabbix Official Repository - $basearch'
  baseurl "#{node['sysadmin']['yum_repo_url']}/Ez-Zabbix/zabbix/#{node['ez-zabbix']['version']}/rhel/5/$basearch/"
  enabled  true
  gpgcheck false
  metadata_expire node['ez-yum']['metadata_expire']
  action   :create
  only_if 'grep "release 5." /etc/redhat-release'
end
yum_repository 'Ez-Zabbix' do
  description 'Ez Zabbix Official Repository - $basearch'
  baseurl "#{node['sysadmin']['yum_repo_url']}/Ez-Zabbix/zabbix/#{node['ez-zabbix']['version']}/rhel/6/$basearch/"
  enabled  true
  gpgcheck false
  metadata_expire node['ez-yum']['metadata_expire']
  action   :create
  only_if 'grep "release 6." /etc/redhat-release'
end
yum_repository 'Ez-Zabbix-Non-Supported' do
  description 'Ez Zabbix Official Repository Non-Supported - $basearch'
  baseurl "#{node['sysadmin']['yum_repo_url']}/Ez-Zabbix/zabbix-non-supported/rhel/6/$basearch/"
  enabled  true
  gpgcheck false
  metadata_expire node['ez-yum']['metadata_expire']
  action   :create
end
