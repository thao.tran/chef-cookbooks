#
# Cookbook Name:: ez-yum
# Recipe:: ez-saas
#
# Maintainer:: Thao Tran
# Copyright 2018, Ez LLC.
# All rights reserved - Do Not Redistribute
#

yum_repository 'Ez-SaaS' do
  description 'Ez SaaS Official Repository - $basearch'
  baseurl "#{node['sysadmin']['yum_repo_url']}/Ez-SaaS/CentOS/$releasever/$basearch/"
  gpgcheck false
  metadata_expire node['ez-yum']['metadata_expire']
  enabled  node['ez-yum']['ez-saas']['enabled']
  action   :create
end

yum_repository 'Ez-Core' do
  description 'Ez Core Official Repository - $basearch'
  baseurl "#{node['sysadmin']['yum_repo_url']}/Ez-Core/"
  gpgcheck false
  metadata_expire node['ez-yum']['metadata_expire']
  enabled  node['ez-yum']['ez-saas']['enabled']
  action   :create
end

yum_repository 'Ez-OnDemand' do
  description 'Ez OnDemand Official Repository - $basearch'
  baseurl "#{node['sysadmin']['yum_repo_url']}/Ez-OnDemand/"
  gpgcheck false
  metadata_expire node['ez-yum']['metadata_expire']
  enabled  node['ez-yum']['ez-saas']['enabled']
  action   :create
end