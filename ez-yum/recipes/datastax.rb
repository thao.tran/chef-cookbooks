#
# Cookbook Name:: ez-yum
# Recipe:: datastax
# Maintainer:: Thao Tran
# Copyright 2018, Ez LLC.
# All rights reserved - Do Not Redistribute
#

include_recipe "ez-yum::default"

yum_repository 'datastax' do
  description 'DataStax Repo for Apache Cassandra'
  baseurl 'http://rpm.datastax.com/community'
  gpgcheck false
  enabled true
  action :create
end
