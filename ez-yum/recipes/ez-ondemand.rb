#
# Cookbook Name:: ez-yum
# Recipe:: ez-ondemand
#
# Maintainer:: Thao Tran
# Copyright 2018, Ez LLC.
# All rights reserved - Do Not Redistribute
#

yum_repository node['ez-yum']['ondemand']['name'] do
  description "Ez OnDemand Official Repository - $basearch"
  baseurl node['ez-yum']['ondemand']['baseurl']
  metadata_expire node['ez-yum']['metadata_expire']
  gpgcheck node['ez-yum']['ondemand']['gpgcheck']
  action   :create
end
