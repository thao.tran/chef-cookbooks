#
# Cookbook Name:: ez-yum
# Recipe:: nginx
#
# Maintainer:: Thao Tran
# Copyright 2018, Ez LLC.
# All rights reserved - Do Not Redistribute
#

include_recipe 'ez-yum::default'

platform = node['platform']
base_url = node['sysadmin']['yum_repo_url']
nginx_url = "#{base_url}/io-nginx/#{platform}/$releasever/$basearch"

unless %w(centos redhat).include? platform
  fail "#{platform} not supported"
end

yum_repository 'Ez-Nginx' do
  description 'Ez Nginx Official Repository - $basearch'
  baseurl nginx_url
  gpgcheck false
  metadata_expire node['ez-yum']['metadata_expire']
  enabled true
  action :create
end
