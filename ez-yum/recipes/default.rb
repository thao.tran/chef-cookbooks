#
# Cookbook Name:: ez-yum
# Recipe:: default
#
# Maintainer:: Thao Tran
# Copyright 2018, Ez LLC.
# All rights reserved - Do Not Redistribute
#

include_recipe 'ez-yum::itson-centos'
include_recipe 'ez-yum::itson-saas'

template "/etc/yum.conf" do
   source "yum.conf.erb"
   owner "root"
   group "root"
   mode 0644
end
