#
# Cookbook Name:: ez-yum
# Recipe:: centos6-scl
# Maintainer:: Thao Tran
# Copyright 2018, Ez LLC.
# All rights reserved - Do Not Redistribute
#

include_recipe "ez-yum::default"

yum_repository 'Ez-CentOS6-SCL' do
  description 'Ez CentOS6-SCL Local Mirror - $basearch'
  baseurl "#{node['sysadmin']['yum_repo_url']}/CentOS6-SCL/"
  gpgcheck false
  enabled  node['ez-yum']['centos6-scl']['enabled']
  action   :create
end
