#
# Cookbook Name:: ez-yum
# Recipe:: centos6-rhel
# Maintainer:: Thao Tran
# Copyright 2018, Ez LLC.
# All rights reserved - Do Not Redistribute
#

include_recipe "ez-yum::default"

execute 'enable/disable rhsm.manage_repos' do
  command "subscription-manager config --rhsm.manage_repos=#{node['ez-yum']['rhsm_manage_repos']}"
end

yum_repository 'Ez-CentOS6-RHEL' do
  description 'Ez CentOS6-RHEL Local Mirror - $basearch'
  baseurl "#{node['sysadmin']['yum_repo_url']}/CentOS6-RHEL/"
  gpgcheck false
  metadata_expire node['ez-yum']['metadata_expire']
  enabled  true
  action   :create
end
