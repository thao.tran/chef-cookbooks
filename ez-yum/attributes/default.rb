default['sysadmin']['proxy_url']           = ""
default['sysadmin']['yum_repo_url']        = "http://yum:800/yum"

default['ez-yum']['hostname']              = 'yum'
default['ez-yum']['gpgcheck']              = "0"
default['ez-yum']['rhsm_manage_repos']     = "0" # Set 1 to enable rhsm management of repos

default['ez-yum']['hdp_version']           = '2.3.4.0'
default['ez-yum']['hdp_utils_version']     = '1.1.0.20'
default['ez-yum']['ambari_version']        = '2.2.0.0'

default['ez-yum']['core']['name']          = 'ez-core'
default['ez-yum']['core']['baseurl']       = 'http://yum:800/yum/ez-core'
default['ez-yum']['core']['gpgcheck']      = false

default['ez-yum']['sapphire']['name']      = 'ez-sapphire'
default['ez-yum']['sapphire']['baseurl']   = 'http://yum:800/yum/ez-sapphire'
default['ez-yum']['sapphire']['gpgcheck']  = false

default['ez-yum']['ondemand']['name']      = 'ez-ondemand'
default['ez-yum']['ondemand']['baseurl']   = 'http://yum:800/yum/ez-ondemand'
default['ez-yum']['ondemand']['gpgcheck']  = false

default['ez-yum']['metadata_expire']       = '1'
default['ez-yum']['bintray_repos']         = ['core']
default['ez-yum']['enable_bintray']        = false
default['ez-yum']['ez-saas']['enabled']    = true
