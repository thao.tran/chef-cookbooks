name             'ez-yum'
maintainer       'Nate Faerber, Thao Tran'
maintainer_email 'nate.faerber@ez.com, thao.tran@ez.com'
license          'All rights reserved'
description      'Installs/Configures yum repos.'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.5.6'

depends "yum", "<= 3.2.2"
